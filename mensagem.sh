#!/usr/bin/sh
# Escrito por: Thomaz Rodrigues Botelho
# Descrição: Altera o fundo de tela para um fundo com uma mensagem
#            customizada para que apareça quando estiver na tela
#            bloqueada do Gnome 3.

if [ "$#" == "0" ]; then
  echo "Uso: mensagem.sh [OPÇÃO]"
  echo "Configura a mensagem de tela bloqueada. Se não for especificada opção a mensagem atual será removida."
  echo ""
  echo "Opções válidas:"
  echo "al - Saí para almoçar"
  echo "bc - Estudando na Biblioteca Central"
  echo "ca - Estou em casa"
  echo "jv - Já volto"
  echo "pe - Aula de Processos Estocásticos"
  echo "rm - Aula de Robótica Móvel"
  echo "rr - Aula de Robótica de Reabilitação"
  echo "sl - Aula de Sistemas Lineares"
  echo ""
  echo "-> Mensagem atual removida!"
  cp ~/Imagens/wallpaper-vazio.jpg ~/Imagens/wallpaper.jpg
else
  case $1 in
    "al") echo "-> Mensagem configurada: Saí para almoçar";
          cp ~/Imagens/wallpaper-al.jpg ~/Imagens/wallpaper.jpg;;
    "bc") echo "-> Mensagem configurada: Estudando na Biblioteca Central";
          cp ~/Imagens/wallpaper-bc.jpg ~/Imagens/wallpaper.jpg;;
    "ca") echo "-> Mensagem configurada: Estou em casa";
          cp ~/Imagens/wallpaper-ca.jpg ~/Imagens/wallpaper.jpg;;
    "jv") echo "-> Mensagem configurada: Já volto";
          cp ~/Imagens/wallpaper-jv.jpg ~/Imagens/wallpaper.jpg;;
    "pe") echo "-> Mensagem configurada: Aula de Processos Estocásticos";
          cp ~/Imagens/wallpaper-pe.jpg ~/Imagens/wallpaper.jpg;;
    "rm") echo "-> Mensagem configurada: Aula de Robótica Móvel";
          cp ~/Imagens/wallpaper-rm.jpg ~/Imagens/wallpaper.jpg;;
    "rr") echo "-> Mensagem configurada: Aula de Robótica de Reabilitação";
          cp ~/Imagens/wallpaper-rr.jpg ~/Imagens/wallpaper.jpg;;
    "sl") echo "-> Mensagem configurada: Aula de Sistemas Lineares";
          cp ~/Imagens/wallpaper-sl.jpg ~/Imagens/wallpaper.jpg;;
       *) echo "Opção desconhecida! Digite: "mensagem.sh" sem parâmetros para visualizar lista de opções"
          echo "-> Mensagem atual removida!";
          cp ~/Imagens/wallpaper-vazio.jpg ~/Imagens/wallpaper.jpg;;

  esac
fi

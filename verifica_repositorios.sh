#!/usr/bin/sh
# Escrito por: Thomaz Rodrigues Botelho
# Descrição: Verifica os respositórios git da máquina. Os repositórios
#            devem estar no arquivo ~/repositórios.txt um em cada linha
#
# Color   Foreground  Background
# Black   30          40
# Red     31          41
# Green   32          42
# Yellow  33          43
# Blue    34          44
# Magenta 35          45
# Cyan    36          46
# White   37          47
#
# 0 Normal Characters
# 1 Bold Characters
# 4 Underlined Characters
# 5 Blinking Characters
# 7 Reverse video Characters
#
# echo -e "\033[COLORm Sample text"
# echo -e "\e[COLORm Sample text"
# -e enables the escape sequences
#

if [ -e ~/repositorios.txt ]; then
  echo -e "\e[37m============================\e[0m"
  echo -e "\e[37mProcessando \e[1m$(cat ~/repositorios.txt | wc -l) \e[0;37mrepositórios:\e[0m"

  cat ~/repositorios.txt | while read repo; do
    echo -e "\e[37m----------------------------\e[0m"
    echo -e "\e[37mRepositório: \e[1m$repo\e[0m"
    eval "cd $repo 2> /dev/null"
    # $? -> variavel interna que armazena o codigo de erro do programa executado
    if [ $? -gt 0 ]; then
      echo -e "\e[37m- \e[31;1mERRO: Diretório não encontrado\e[0m"
    else
      git status > /dev/null 2> /dev/null
      if [ $? -gt 0 ]; then
        echo -e "\e[37m- \e[31;1mERRO: Diretório não é um repositório\e[0m"
      else
        GitPush=0
        x=$(git status | grep 'Untracked files'|wc -l)
        if [ "$x" -eq 1 ]; then
          echo -e "\e[37m- \e[31malguns arquivos não estão no repositório, verifique manualmente.\e[0m"
        fi
        x=$(git status | grep 'Changes not staged for commit'|wc -l)
        if [ "$x" -eq 1 ]; then
          echo -e "\e[37m- \e[31malguns arquivos foram modificados, verifique manualmente.\e[0m"
        fi
        x=$(git status | grep 'Changes to be committed' |wc -l)
        if [ "$x" -eq 1 ]; then
          echo -e "\e[37m- \e[31mfalta dar commit, verifique manualmente.\e[0m"
        fi
        x=$(git status | grep 'Your branch is ahead of' |wc -l)
        if [ "$x" -eq 1 ]; then
          echo -e "\e[37m- \e[33mFalta enviar commit para repositório.\e[0m"
          echo -e "\e[37m>> Executando push para enviar:\e[0m"
          git push origin master
          echo -e "\e[37m>> Push executado!\e[0m"
          GitPush=1
        fi
        x=$(git status | grep 'nothing to commit' |wc -l)
        if [ "$x" -eq 1 ]; then
          echo -e "\e[37m- \e[32mrepositório OK\e[0m"
          if [ "$GitPush" -eq 0 ]; then
            echo -e "\e[37m>> Executando pull por precaução:\e[0m"
            git pull origin master
            echo -e "\e[37m>> Pull executado!\e[0m"
          fi
        fi
      fi
    fi
  done
  echo -e "\e[37m============================\e[0m"
  echo -e "\e[37mProcessados \e[1m$(cat ~/repositorios.txt | wc -l) \e[0;37mrepositórios.\e[0m"
else
  echo -e "\e[37mArquivo \e[31m~/repositorios.txt\e[37m não existe\e[0m"
  echo -e "\e[37mCrie este arquivo contendo os caminhos para cada repositório, um em cada linha.\e[0m"
fi
